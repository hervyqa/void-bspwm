# update
sudo xbps-install -Suv

# wndow manager and utility
sudo xbps-install -Sy acpi autox \
    bspwm colord dunst \
    elogind polkit elogind gtk-engine-murrine \
    gvfs gvfs-mtp hsetroot lemonbar ntfs-3g \
    rofi slop sed sox sxhkd tlp wget xbacklight \
    xdo xinput xprop xrdb xsel xset xsetroot \
    xsettingsd xtitle xtools xclip yank \

# input
sudo xbps-install -Sy xf86-input-evdev \
    xf86-input-joystick xf86-input-libinput \
    xf86-input-mtrack xf86-input-synaptics \
    xf86-input-vmmouse xf86-input-wacom

# xorg
sudo xbps-install -Sy xf86-video-ati \
    xf86-video-intel xorg-fonts xorg-minimal \
    xorg-input-drivers xorg-video-drivers

# kvantum theme
sudo xbps-install -Sy kvantum qt5ct \
    qt5-styleplugins

# cursor & theme
sudo xbps-install -Sy Adapta adapta-kde \
    papirus-icon-theme breeze-obsidian-cursor-theme \
    breeze-snow-cursor-theme

# icon
sudo xbps-install -Sy font-awesome5 \

# fonts
sudo xbps-install -Sy noto-fonts-ttf \
    liberation-fonts-ttf cantarell-fonts \

# shell
sudo xbps-install -Sy zsh zsh-autosuggestions \
    zsh-completions zsh-syntax-highlighting

# gtk appareance setting
sudo xbps-install -Sy lxappearance \

# display setting
sudo xbps-install -Sy arandr autorandr

# compressed
sudo xbps-install -Sy atool p7zip \
    zip unzip lz4 lzop lzip archiver

# codec movie
sudo xbps-install -Sy gst-plugins-base1 \
    gst-plugins-good1 gst-plugins-ugly1 \
    gst-plugins-bad1 gst-libav

# codec audio
sudo xbps-install -Sy faac faad2 flac \
    jasper lame libdca libmpeg2 libmad \
    libtheora libvorbis libXv wavpack \
    x264 xvidcore gstreamer-vaapi \
    alsa-utils alsa-tools alsa-utils sox

# compiler
sudo xbps-install -Sy automake binutils \
    bison fakeroot flex gcc libtool m4 \
    make patch pkg-config guile

# terminal emulator
sudo xbps-install -Sy rxvt-unicode tmux \

# text editor cli
sudo xbps-install -Sy neovim \
    python3-neovim \

# text editor gui
sudo xbps-install -Sy leafpad \

# file manager cli
sudo xbps-install -Sy ranger exiftool \
    w3m w3m-img odt2txt jq transmission

# file manager gui
sudo xbps-install -Sy Thunar \
    thunar-archive-plugin thunar-media-tags-plugin \
    thunar-volman file-roller tumbler

# browser
sudo xbps-install -Sy icecat

# image viewer
sudo xbps-install -Sy vimiv

# media player
sudo xbps-install -Sy mediainfo \
    mpc mpd mpv ncmpcpp

# monitor system
sudo xbps-install -Sy htop ytop \
    glances inxi

# screenshot
sudo xbps-install -Sy scrot

# pdf viewer
sudo xbps-install -Sy mupdf \

# recording video
sudo xbps-install -Sy ffmpeg xdpyinfo \
    ssr

# recording terminal
sudo xbps-install -Sy asciinema

# keyboard monitor
sudo xbps-install -Sy key-mon

# vcs
sudo xbps-install -Sy git

# backup
sudo xbps-install -Sy borg

# android
sudo xbps-install -Sy android-tools jmtpfs \

# yt download
sudo xbps-install -Sy youtube-dl

# bootable linux
sudo xbps-install -Sy bootiso \
    mtools gptfdisk efibootmgr dosfstools

# printers
sudo xbps-install -Sy cups \
    cups-filters gutenprint \

# bootable windows
sudo xbps-install -Sy winusb

# typing
sudo xbps-install -Sy gtypist

## services
sudo ln -s /etc/sv/acpid /var/service/
sudo ln -s /etc/sv/dbus /var/service/
sudo ln -s /etc/sv/dhcpcd /var/service/
sudo ln -s /etc/sv/elogind /var/service/
sudo ln -s /etc/sv/polkitd /var/service/
sudo ln -s /etc/sv/sshd /var/service/
sudo ln -s /etc/sv/tlp /var/service/
sudo ln -s /etc/sv/udevd /var/service/
sudo ln -s /etc/sv/uuidd /var/service/
sudo ln -s /etc/sv/wpa_supplicant /var/service/
# sudo ln -s /etc/sv/autox /var/service/
sudo rm /var/service/agetty-tty3
sudo rm /var/service/agetty-tty4
sudo rm /var/service/agetty-tty5
sudo rm /var/service/agetty-tty6

# office suite
sudo xbps-install -Sy \
    libreoffice-i18n-en-US \
    libreoffice-fonts \
    libreoffice-base \
    libreoffice-gnome \
    libreoffice-impress \
    libreoffice-draw \
    libreoffice-math \
    libreoffice-writer \
    libreoffice-calc \

# fonts
sudo xbps-install -Sy google-fonts-ttf

## design audio
sudo xbps-install -Sy audacity \

# design vector
sudo xbps-install -Sy inkscape \
    icoutils ghostscript jpegoptim optipng \
    pngquant libwebp-tools ImageMagick librsvg \

# inxscape extension
sudo xbps-install -Sy librsvg-utils \
    pdftk zenity fig2dev \
    python3-lxml python3-numpy

# design bitmap
sudo xbps-install -Sy gimp \

# design animation
sudo xbps-install -Sy blender \

# web development
sudo xbps-install -Sy hugo \

# void mklive
sudo xbps-install -Sy liblz4 lz4 \
    qemu-user-static

# wine
sudo xbps-install -Sy wine wine-gecko \
    wine-mono winetricks

# default shell zsh
chsh -s /usr/bin/zsh

# last step manually

# audio
sudo usermod -a -G audio hervyqa
