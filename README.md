## Introduction
A repository contains personal backup of my Void Linux setup.

## Screenshot

![hervyqa-1](img/hervyqa-1.png)
![hervyqa-2](img/hervyqa-2.png)

## Details
- **Desktop Environment :** None
- **Graphical Server :** Xorg Minimal
- **Video Driver :** xf86-video-intel, xf86-video-ati
- **Touchpad Driver :** Synaptics
- **Display Manager :** autox
- **Window Manager** : Bspwm
- **Panel :** Lemonbar
- **Wallpaper Handler :** hsetroot
- **Program Launcher** : Rofi
- **Settings Daemon** : xsettingsd
- **File Manager CLI :** ranger
- **File Manager GUI :** Thunar
- **Image Viewer :** vimiv
- **Screenshooter :** Scrot
- **Web Browser :** vimb
- **Terminal :** URxvt
- **Terminal Font :** liberation Mono
- **CLI Text Editor :** Neovim
- **GUI Text Editor :** -
- **CLI Music Player :** ncmpcpp
- **GUI Music Player :** -
- **Multimedia Player :** Mpv
- **Encoder/Decoder :** ffmpeg
- **PDF Reader :** Mupdf
- **Archive Manager CLI:** arc
- **Archive Manager GUI:** File Roller
- **IRC Client :** weechat
- **GTK Engine :** Murrine
- **GTK Theme :** Adapta Nokto Eta
- **GTK Icons :** Papirus Dark
- **GTK Cursors :** Breeze Obsidian
- **GTK Font :** Liberation Mono
- **CLI Shell :** Zsh
- **Notification Daemon :** Dunst
- **Sound Mixer :** ALSA
- **Task Manager :** ytop
- **Init :** runit
- **SystemD :** No
- **C Library :** GLibC
- **Login :** Elogind

## Original Credit Dotfiles
- [addy-dclxvi](https://github.com/addy-dclxvi/void-bspwm-dotfiles)
