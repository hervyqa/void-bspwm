" ------ enable additional settings ------

set modeline           " enable vim modelines
set hlsearch           " highlight search items
set incsearch          " searches are performed as you type
set number             " enable line numbers
set confirm            " ask confirmation like save before quit.
set wildmenu           " Tab completion menu when using command mode
set expandtab          " Tab key inserts spaces not tabs
set softtabstop=4      " spaces to enter for each tab
set shiftwidth=4       " amount of spaces for indentation
set shortmess+=aAcIws  " Hide or shorten certain messages
set noswapfile         " No swap
set nobackup           " No backup
set noerrorbells       " No annoying
set novisualbell       " Don't blink
set relativenumber     " scroll number
set ruler
set scrolloff=5        " Keep 10 lines (top/bottom) for scope
set splitbelow         " Vertical split to below
set splitright         " Horizontal split to right
set smarttab
set updatetime=100     " Quickly realtime update
set undofile
set undolevels=50000
set t_Co=256           " set terminal 256 color
let g:netrw_altv = 1
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3
let g:mapleader = "\<Space>"

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showmode
set showcmd
set cmdheight=1

" Highlight matching pairs of brackets. Use the '%' character to jump between them.
set matchpairs+=<:>

" Display different types of white spaces.
" set list
" set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number
highlight LineNr ctermfg=black

" Set status line display
set laststatus=2
hi StatusLine ctermfg=black ctermbg=NONE cterm=NONE
hi StatusLineNC ctermfg=black ctermbg=black cterm=NONE
hi User1 ctermfg=NONE ctermbg=red
hi User2 ctermfg=NONE ctermbg=blue
set statusline=%=%1*    " Switch to right-side
set statusline+=\ \     " Padding
set statusline+=%f      " Path to the file (short)
set statusline+=\ %2*\  " Padding & switch colour
set statusline+=%l      " Current line
set statusline+=\       " Padding
set statusline+=of      " of text
set statusline+=\       " Padding
set statusline+=%L      " Current line
set statusline+=\       " Padding

" Encoding
set encoding=utf-8

" Highlight matching search patterns
set hlsearch

" Enable incremental search
set incsearch

" Include matching uppercase words with lowercase search term
set ignorecase

" Include only uppercase words with uppercase search term
set smartcase

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

" ------ enable additional features ------

" enable mouse
set mouse=a
if has('mouse_sgr')
    " sgr mouse is better but not every term supports it
    set ttymouse=sgr
endif

" syntax highlighting
syntax enable

set linebreak breakindent
set list listchars=tab:>>,trail:~

" change windows with ctrl+(hjkl)
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
nnoremap <BS> <C-W><C-H>

" alt defaults
nnoremap 0 ^
nnoremap Y y$
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap <Tab> ==1j

" re-visual text after changing indent
vnoremap > >gv
vnoremap < <gv

" toggle line numbers, nn (no number)
nnoremap <silent> <Leader>nn :set number!<CR>

" nohlsearch
nnoremap <silent> <Leader><cr> :nohlsearch<CR>

" close/exit current buffer and/or tab
nnoremap <silent> <Leader>q :q<CR>

" force close buffer
nnoremap <silent> <leader>Q :bd<CR>

" open a new tab in the current directory with netrw
nnoremap <silent> <Leader>- :tabedit <C-R>=expand("%:p:h")<CR><CR>

" split the window vertically and horizontally
nnoremap _ <C-W>s<C-W><Down>
nnoremap <Bar> <C-W>v<C-W><Right>

" quick reload .vimrc
nnoremap <silent> <leader>r :source $MYVIMRC<CR>

" save
nnoremap <silent> <leader>w :w<CR>

" equally
map = <C-w>=

" arrow keys resize windows
nnoremap <silent> <Left> :vertical resize +2<CR>
nnoremap <silent> <Right> :vertical resize -2<CR>
nnoremap <silent> <Up> :resize +2<CR>
nnoremap <silent> <Down> :resize -2<CR>

