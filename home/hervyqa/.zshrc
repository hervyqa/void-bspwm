#!/bin/zsh

# highlighting
if [[ -f /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]]; then
  . /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi

autoload -U compinit && compinit

# catch non-zsh and non-interactive shells
[[ $- == *i* && $ZSH_VERSION ]] && SHELL=/usr/bin/zsh || return 0

# set some defaults
export MANWIDTH=90
export HISTSIZE=10000
export SAVEHIST=10000

# set for terminal
export TERMINAL="urxvt"
export EDITOR="nvim"

# path to the history
HISTFILE=~/.histfile

# add ~/bin to the path if not already, the -U flag means 'unique'
typeset -U path=($HOME/.bin "${path[@]:#}")

# promt
PROMPT='%F{cyan}%1~%f %# '

# initialize the prompt
autoload -U promptinit && promptinit

# disable auto update
DISABLE_AUTO_UPDATE="true"

# eanble auto correction
ENABLE_CORRECTION="true"

# disable marking untracked files
DISABLE_UNTRACKED_FILES_DIRTY="true"

# enabled plugin
plugins=(
  git
)

# # tmux
# if [[ -z "$TMUX" ]]; then
#     tmux attach
# fi

## Some useful alias to make your life easier :)

# open text editor
if hash nvim >/dev/null 2>&1; then
    alias vim='nvim'
    alias v='nvim'
    alias sv='sudo nvim'
else
    alias v='vim'
    alias sv='sudo vim'
fi

# open task
alias tt="task"

# pin voidlinux.org
alias pp="ping -c3 voidlinux.org"

# lsblk
alias disk="sudo lsblk -o NAME,FSAVAIL,FSUSE%,SIZE,TYPE,LABEL,MOUNTPOINT"

## refresh font cache
alias font-refresh="fc-cache -fv"

## git clone depth 1, who needs to clone full repository if you can clone the top layer only
alias clone="git clone --depth 1"

# always use neovim
command -v nvim >/dev/null && alias vim="nvim" vimdiff="nvim -d"

# tmux
alias \
    tm="tmux" \
    tma="tmux attach" \

# verbosity and settings that you pretty much just always are going to want.
alias \
    cp="cp -iv" \
    mv="mv -iv" \
    rm="rm -vI" \
    mkd="mkdir -pv" \
    yt="youtube-dl --add-metadata -i" \
    yta="yt -x -f bestaudio/best" \
    ffmpeg="ffmpeg -hide_banner"

# xbps
alias \
    search="xbps-query -v -Rs" \
    dependency="xbps-query -Rx" \
    sync="sudo xbps-install -S" \
    upgrade="sudo xbps-install -Suv; sudo xbps-remove -v -Oo" \
    update="sudo xbps-install -uv; sudo xbps-remove -v -Oo" \
    install="sudo xbps-install" \
    forceinstall="sudo xbps-install -Sf" \
    info="sudo xbps-query -S" \
    remove="sudo xbps-remove -R" \
    forceremove="sudo xbps-remove -R -f" \
    autoremove="sudo xbps-remove -v -Oo" \
    filelist="xbps-query --regex -Rf" \
    repolist="xbps-query -v -L"

# [ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
